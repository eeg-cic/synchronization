rm(list=ls())
cat("\014")

script.dir <- dirname(sys.frame(1)$ofile)
setwd(script.dir)

library('tidyverse')

print('Begin synchronisation operation for gtec')

# eeg_file <- 'LG03DataGtech.csv'
# ppg_file <- 'LG03PhysioGtech&Emotiv.csv'

eeg_file <- readline(prompt="Enter a response file name : ")
ppg_file <- readline(prompt="Enter a physio file name : ")

a <- read_csv2(eeg_file,col_types = 'nnnnn')
a <- select(a,c(2,3,4))
# a <- a[rowSums(is.na(a[,2:3]))!=2,]
a <- rename(a, time=colnames(a)[1])


b <- read_tsv(ppg_file,skip=1)

time_holder <- str_locate(colnames(b),'TimestampSync')
for(i in 1:length(b)){
  if(!is.na(time_holder[i])){
    time_place = i
  }
}

skin_holder <- str_locate(colnames(b),'Skin_Conductance')
for(i in 1:length(b)){
  if(!is.na(skin_holder[i])){
    skin_place = i
  }
}

ppg_holder <- str_locate(colnames(b),'PPGtoHR')
for(i in 1:length(b)){
  if(!is.na(ppg_holder[i])){
    ppg_place = i
  }
}

ibi_holder <- str_locate(colnames(b),'PPG_IBI')
for(i in 1:length(b)){
  if(!is.na(ibi_holder[i])){
    ibi_place = i
  }
}

# b <- select(b,c('Shimmer_92FA_TimestampSync_Unix_CAL','Shimmer_92FA_GSR_Skin_Conductance_CAL','Shimmer_92FA_PPGtoHR_CAL'))
b <- select(b,c(time_place,skin_place,ppg_place,ibi_place))
b <- b[2:nrow(b),]
b <- rename(b, time=colnames(b)[1])
b <- mutate(b,time = as.double(time))

c <- bind_rows(a,b)
c <- arrange(c,time)

first <- filter(c, Musique==1)[[1,1]]
first <- which(c$time == first)

if (length(first) > 1){
  first <- first[1]
}

last <- filter(c, Musique==8)[[2,1]]
last <- which(c$time == last)

if (length(last) > 1){
  last <- last[2]
}

c <- slice(c,first:last)
# write.csv(c , file = paste(strsplit(eeg_file,'.csv')[[1]],'_Physio_synchronized','.csv',sep=''),row.names = FALSE,na='')







fillNAgaps <- function(x, firstBack=FALSE) {
  ## NA's in a vector or factor are replaced with last non-NA values
  ## If firstBack is TRUE, it will fill in leading NA's with the first
  ## non-NA value. If FALSE, it will not change leading NA's.
  
  # If it's a factor, store the level labels and convert to integer
  lvls <- NULL
  if (is.factor(x)) {
    lvls <- levels(x)
    x    <- as.integer(x)
  }
  
  goodIdx <- !is.na(x)
  
  # These are the non-NA values from x only
  # Add a leading NA or take the first good value, depending on firstBack   
  if (firstBack)   goodVals <- c(x[goodIdx][1], x[goodIdx])
  else             goodVals <- c(NA,            x[goodIdx])
  
  # Fill the indices of the output vector with the indices pulled from
  # these offsets of goodVals. Add 1 to avoid indexing to zero.
  fillIdx <- cumsum(goodIdx)+1
  
  x <- goodVals[fillIdx]
  
  # If it was originally a factor, convert it back
  if (!is.null(lvls)) {
    x <- factor(x, levels=seq_along(lvls), labels=lvls)
  }
  
  x
}

c[,2] <- fillNAgaps(c[[2]],firstBack = TRUE)
d <- filter(c,c['Rp.']==0)

for(i in 1:4){
  temp_rp <- filter(c,c['Rp.']==i)
  name_holder <- paste('Rp.',i,sep='')
  temp_rp <- rename(temp_rp, !!name_holder:='Rp.')
  d <- bind_rows(d,temp_rp)
}

d <- arrange(d,time)
d <- select(d,c(1,7,8,9,10,3,4,5,6))
d <- replace_na(d,list('Rp.1'=0))
d <- replace_na(d,list('Rp.2'=0))
d <- replace_na(d,list('Rp.3'=0))
d <- replace_na(d,list('Rp.4'=0))

d[1,7:8] <- 0
d[nrow(d),7:8] <- 0

for(i in 2: nrow(d)-1){
  if(is.na(d[i,7])){
    
    if(is.na(d[i-1,7]) || is.na(d[i+1,7])){
      #no operation
    }
    else{
      d[i,7] <- (as.double(d[i-1,7])+as.double(d[i+1,7]))/2
      d[i,8] <- (as.double(d[i-1,8])+as.double(d[i+1,8]))/2
    }
    
  }
}
d[, 6:8][is.na(d[, 6:8])] <- 0
d[,9][d[,9]=='-1.0'] <- NA
d[,9][is.na(d[,9])] <- 0
write.csv(d , file = paste(strsplit(eeg_file,'.csv')[[1]],'_Physio_synchronized','.csv',sep=''),row.names = FALSE,na='')

e <- tibble(emotion=rowSums(d[,2:5]),ibi=as.numeric(unlist(d[,colnames(d)[9]])))
e <- filter(e, e$ibi > 0 )

moving_fun <- function(x, w, FUN, ...) {
  # x: a double vector
  # w: the length of the window, i.e., the section of the vector selected to apply FUN
  # FUN: a function that takes a vector and return a summarize value, e.g., mean, sum, etc.
  # Given a double type vector apply a FUN over a moving window from left to the right, 
  #    when a window boundary is not a legal section, i.e. lower_bound and i (upper bound) 
  #    are not contained in the length of the vector, return a NA_real_
  if (w < 1) {
    stop("The length of the window 'w' must be greater than 0")
  }
  output <- x
  for (i in 1:length(x)) {
    # plus 1 because the index is inclusive with the upper_bound 'i'
    lower_bound <- i - w + 1
    if (lower_bound < 1) {
      output[i] <- NA_real_
    } else {
      output[i] <- FUN(x[lower_bound:i, ...])
    }
  }
  output[2] <- 0
  output <- append(output,0)
  output <- output[2:length(output)]
  return(output)
}

# e$ibi <- moving_fun(e$ibi,3,mean)
mean_ibi <- tibble(mean_ibi=moving_fun(e$ibi,3,mean))
e <- bind_cols(e,mean_ibi)

different <- function(a){
  return(a[2]-a[1])
}

HRV <- tibble(HRV = moving_fun(e$mean_ibi,2,different))

e <- bind_cols(e,HRV)
e[nrow(e)-1,'HRV'] <- 0

write.csv(e , file = paste(strsplit(eeg_file,'.csv')[[1]],'_HRV','.csv',sep=''),row.names = FALSE,na='')
print('Finished the operation')